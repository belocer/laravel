<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', function () {
	return view( 'welcome' );
} );

/*Route::get('/page', function () {
	echo '<pre>';
	print_r($_ENV);
	Config::set('app.locale', 'ru');
	echo config('app.locale');
	echo '</pre>';
	return view('page');
});*/

Route::post( '/comments', function () {

	return view( 'comments' );

} );
